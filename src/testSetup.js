import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import 'mock-local-storage'
import { setupI18n } from './utils/testUtils'
import { createSerializer } from 'enzyme-to-json'

expect.addSnapshotSerializer(createSerializer({mode: 'shallow'}))
setupI18n()

configure({ adapter: new Adapter() })
