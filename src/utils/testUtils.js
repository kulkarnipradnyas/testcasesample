import * as path from 'path'
import i18n from 'i18next'
import i18nBackend from 'i18next-sync-fs-backend'
import React from 'react'
import { shallow } from 'enzyme'

/**
 * create a setup function for testing components
 */
export const getComponentSetup = (Component, defaultProps) =>
  props => ({
    props: { ...defaultProps, ...props },
    shallowWrapper: shallow(<Component {...defaultProps} {...props} />)
  })

/**
 * Set up i18n instance for testing with sync FS loader
 */
export function setupI18n () {
  i18n
    .use(i18nBackend)
    .init({
      lng: 'en',
      fallbackLng: 'en',

      ns: ['translations'],
      defaultNS: 'translations',

      debug: false,
      saveMissing: false,

      initImmediate: false,

      interpolation: {
        escapeValue: false // not needed for react!!
      },

      backend: {
        loadPath: path.resolve(__dirname, '../../public/locales/{{lng}}/{{ns}}.json')
      }
    })
}

/**
 * Return simplified version of i18next instance with only `t` function
 * in order to not to dump entire i18next object into enzyme snapshots
 */
export function mockI18n () {
  return {
    t: i18n.t.bind(i18n)
  }
}
